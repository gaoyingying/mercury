# -*- coding: utf-8 -*-

# import sys
# sys.path.insert(0, "/Users/ju/workspace/mercury")

import asyncio

from motor.motor_asyncio import AsyncIOMotorClient

from mercury.utils import md5
from mercury.settings import STORAGE


async def _connect_mongdb():
    conf = STORAGE["mongo"]

    client = AsyncIOMotorClient(
        host=conf["nodes"],
        replicaSet=conf.get("replset"),
        connect=False,
        username=conf.get("username"),
        password=conf.get("password"),
        authSource=conf.get("dbname")
    )
    mgdb = client.mercury
    return mgdb

loop = asyncio.get_event_loop()
mgdb = loop.run_until_complete(_connect_mongdb())


if __name__ == "__main__":

    from sanic import Sanic
    from sanic.response import json
    app = Sanic("test")

    # @app.listener('before_server_start')
    # async def setup_db(app, loop):
        # await _connect_mongdb()

    @app.route("/")
    async def test(request):
        retval = await mgdb.test.find_one()
        print(retval)
        retval.pop('_id')
        return json(retval)

    app.run(host="0.0.0.0", port=8888, debug=False, workers=2)
