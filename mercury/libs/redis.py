# -*- coding: utf-8 -*-

# import sys
# sys.path.insert(0, "/Users/ju/workspace/mercury")

import asyncio
import aioredis

from mercury.settings import STORAGE


async def _connect_redis_pool():
    conf = STORAGE["redis"]
    try:
        redis_pool = await aioredis.create_redis_pool(
            (conf["host"], conf["port"]),
            db=1,
            password=conf["password"],
            encoding="utf8",
            loop=None)
    except:
        print("Redis ConnectionError")
    else:
        return redis_pool

loop = asyncio.get_event_loop()
redis_pool = loop.run_until_complete(_connect_redis_pool())


if __name__ == "__main__":

    async def save():
        await redis_pool.set("testkey", 3)
        return await redis_pool.get("testkey")

    val = loop.run_until_complete(save())
    print(val)

