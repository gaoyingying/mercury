# -*- coding: utf-8 -*-

import hashlib
from datetime import datetime


def md5(obj):
    """计算字符串的 md5 值，可接受 Unicode 或 bytes 对象，
    避免代码中多次自行做类型转换
    """
    if isinstance(obj, str):
        obj = obj.encode('utf8')
    elif not isinstance(obj, bytes):
        raise TypeError("MD5 input must be string.")
    return hashlib.md5(obj).hexdigest()


def timestamp(is_utc=True, is_int=False):
    """返回当前秒级时间戳

    :param is_utc: 是否返回UTC时间，否则返回当地(根据系统配置)时间
    :type is_utc: bool

    :param is_int: 是否返回整数形式，否则返回 datetime 对象
    :type is_int: bool

    :return: 整数或 datetime 对象形式的时间戳
    :rtype: int or datetime
    """
    t = datetime.utcnow() if is_utc else datetime.now()
    return int(t.timestamp()) if is_int else t


class lazyproperty:
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            value = self.func(instance)
            setattr(instance, self.func.__name__, value)
            return value
