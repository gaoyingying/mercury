# -*- coding: utf-8 -*-

"""
基于prometheus的监控指标
"""

from prometheus_client import Counter, Gauge, Histogram, Summary


news_amount = Gauge(
    'news_amount', 'Total number of news crawled from baidunews')

jobs_in_redis = Gauge(
    'jobs_in_redis', 'The number of jobs to be crawled in redis queue')

seeds_amount = Gauge(
    'seeds_amount', 'Total number of seeds added to the database', ["getway"])

zero_seeds_amount = Gauge(
    'zero_seeds_amount', 'Number of seeds that have never been crawled', ["getway"])

jobs_inprogress = Gauge(
    'jobs_inprogress', 'The number of jobs being crawled')

# ############### Counter ##############
enter_processing = Counter(
    'enter_processing', 'The total number of enter the crawler processing')

extract_detail_failures = Counter(
    'extract_detail_failures', 'Total number of failures on detail page goose extraction')

download_failures = Counter(
    'download_failures', 'Total number of failures on http download')

news_saved_to_mongo = Counter(
    'news_saved_to_mongo', 'The total number of news saved to mongodb')

news_duplicated = Counter(
    'news_duplicated', 'Enter the schedule but crawled before')

extract_error_counter = Counter(
    'extract_error_counter', 'Number of execptions when extracting detail page')

extract_nothing_counter = Counter(
    'extract_nothing_counter', 'No execptions when do extract but get nothing')


# ############### Summary ##############
time_on_download = Summary(
    'time_on_download', 'Time spent on downloading a webpage')

time_on_extract_detail = Summary(
    'time_on_extract_detail', 'Time spent on extract a detail webpage')

time_on_extract_index= Summary(
    'time_on_extract_index', 'Time spent on extract a index webpage')

time_on_upload_s3 = Summary(
    'time_on_upload_s3', 'Time spent on upload webpage raw html to ourown s3')

time_on_write_mongo = Summary(
    'time_on_write_mongo', 'Time spent on write data to mongodb')
