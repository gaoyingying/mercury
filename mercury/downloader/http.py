# -*- coding: utf-8 -*-

"""
基于HTTP的页面下载器
"""
import logging
from functools import partial

from aiohttp import ClientSession

from mercury.settings import logger


class AsyncHttpDownloader:

    def __init__(self, url, data=None, **kwargs):
        self.url = url
        self.data = data
        self._session = ClientSession(read_timeout=60, **kwargs)

    async def fetch(self):
        async with self._session as s:
            if self.data:
                method = partial(s.post, data=self.data)
            else:
                method = s.get

            retval = None, None
            try:
                async with method(self.url) as r:
                    # HTTP状态码
                    if r.status < 400:
                        # 为解决goose抽取编码识别错误问题，
                        # 显式返回aiohttp检测的编码
                        retval = await r.text(errors='ignore'), r.get_encoding()
            except Exception as e:
                logger.error(e, exc_info=True)
                logger.error("{0} while request {1}".format(e.__class__, self.url))
            finally:
                return retval
