# -*- coding: utf-8 -*-

import execjs


def pase_content_js(content):
    """解析文书网携带有文书数据的JS代码并返回
    eg: http://wenshu.court.gov.cn/CreateContentJS/CreateContentJS.aspx?DocID=d8952be5-e5a2-4b8b-b554-cccf5824617f

    :param content: wenshu.court.gov.cn 网站返回的创建文书内容的JS代码

    :return infos: 返回JS代码中携带的文书相关信息
    """
    meta_mark = ('caseinfo=JSON.stringify(', ');$(document')
    relate_mark = ('dirData = ', ';if ($("#divTool_Summary')
    html_mark = ('jsonHtmlData = ', '}";')

    def _parse(start, stop, pos=0, fix=0):
        begin = content.index(start, pos) + len(start)
        end = content.index(stop, begin) + fix
        ret = execjs.eval(content[begin:end])
        if isinstance(ret, str):
            ret = execjs.eval(ret)
        return ret, end

    meta_info, pos = _parse(*meta_mark)
    relate_info, pos = _parse(*relate_mark, pos)
    html_info, pos = _parse(*html_mark, pos, 2)
    return meta_info, relate_info, html_info


if __name__ == '__main__':
    import requests
    headers={"User-Agent":("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) "
                           "AppleWebKit/537.36 (KHTML, like Gecko Chrome/67."
                           "0.3396.87 Safari/537.36"})
    url = 'http://wenshu.court.gov.cn/CreateContentJS/CreateContentJS.aspx?DocID=d8952be5-e5a2-4b8b-b554-cccf5824617f'

    resp = requests.get(url, headers=headers)
    ret = pase_content_js(resp.text)
    print(ret)


