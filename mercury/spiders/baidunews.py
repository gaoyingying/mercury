# -*- coding: utf-8 -*-

import re
import logging
from datetime import datetime, timedelta

import pytz
import lxml
import newspaper
from bs4 import BeautifulSoup
from goose3 import Goose
from goose3.text import StopWordsChinese

from ._base import Spider as BaseSpider
from mercury.exceptions import GooseEncodeError
from mercury.settings import logger
from mercury.settings import RK_BDNEWS_PUBINFO
from mercury.utils import md5
from mercury.monitor import extract_error_counter, extract_nothing_counter

PUBLISHER = r'^(?P<publisher>\S+)'
YEAR_MONTH_DAY = r'^(?P<year>\d{4})年(?P<month>\d{2})月(?P<day>\d{2})日'
HOUR_MINUTE = r'^(?P<hour>\d{2}):(?P<minute>\d{2})$'
SHORT_TIME = r'^(?P<digit>\d+)(?P<hmtype>小时|分钟)前$'

PAT_PUBLISHER = re.compile(PUBLISHER)
PAT_YEAR_MONTH_DAY = re.compile(YEAR_MONTH_DAY)
PAT_HOUR_MINUTE = re.compile(HOUR_MINUTE)
PAT_SHORT_TIME = re.compile(SHORT_TIME)


TZ_SH = pytz.timezone('Asia/Shanghai')


class Spider(BaseSpider):
    name = "baidunews"

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": (" Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:60.0)"
                               " Gecko/20100101 Firefox/60.0")
            },
            "cookies": {},
            "sleep_range_before_request": None
        }
        return configs

    @staticmethod
    def _extract_by_newspaper(url, page):
        try:
            article = newspaper.Article(url)
            article.set_html(page)
            article.parse()
        except Exception as e:
            # newspaper 抽取出错后暂时不管
            extract_error_counter.inc()
            logger.warning("newspaper3kError {0}: {1}".format(
                e.__class__.__name__, url))
            logger.error(e, exc_info=True)
        else:
            return {"title": article.title,
                    "content": article.text}

    @staticmethod
    def _extract_by_goose(url, page, encoding):
        g = Goose({"stopwords_class": StopWordsChinese,
                   "strict": False})
        article = None
        try:
            # goose 自行识别编码并处理
            article = g.extract(raw_html=page)
        except (UnicodeEncodeError, LookupError):
            # 若出错，采用传递进来的编码值
            try:
                article = g.extract(
                    raw_html=page.encode(encoding, errors='ignore'))
            except (lxml.etree.ParserError, Exception) as e:
                extract_error_counter.inc()
                logger.warning("GooseError {0}: {1}".format(
                    e.__class__.__name__, url))
        except Exception as e:
            extract_error_counter.inc()
            logger.warning("GooseError {0}: {1}".format(
                e.__class__.__name__, url))
            logger.error(e, exc_info=True)
        if article and article.cleaned_text:
            return {"title": article.title,
                    "content": article.cleaned_text}

    def _extract_html_page(self, page, encoding):
        """extract title and content by given raw html stings
        :param str page: raw html strings
        :param str encoding: raw html char encoding
        :return dict result: main fields of news
        """
        url = self.job.url
        data = (self._extract_by_goose(url, page, encoding)
                or self._extract_by_newspaper(url, page))

        if data and (not data["content"]):
            extract_nothing_counter.inc()
            logger.warning("Nothing was extracted: {0}".format(url))
        elif data and data["content"]:
            return {"url": url, "title": data["title"],
                    "content": data["content"],
                    "getway": "baidunews", "sid": self.job.seed_urlmd5}

    async def handle_detail(self, page, encoding):
        key = RK_BDNEWS_PUBINFO.format(urlmd5=self.job.urlmd5)
        pubinfo = await self.queue.get(key)
        await self.queue.delete(key)

        if not page:
            return None

        if self.executor:
            data = await self.loop.run_in_executor(
                self.executor, self._extract_html_page, page, encoding)
        else:
            data = self._extract_html_page(page, encoding)

        if data and pubinfo:
            publisher, pubtime = pubinfo.split('::')
            data['publisher'] = publisher
            data['pubtime'] = datetime.fromtimestamp(int(pubtime), tz=pytz.utc)
        return data

    async def handle_index(self, page):
        if not page:
            return None, None
        soup = BeautifulSoup(page, 'lxml')
        pages = soup.select('div.result')
        urls = []
        for page in pages:
            cauthor = page.select('p.c-author')[0].get_text(strip=True)
            publisher, pubtime = self._extract_pubinfo(cauthor)
            link = page.select('h3 > a')[0].get('href')
            urlmd5 = md5(link)
            key = RK_BDNEWS_PUBINFO.format(urlmd5=urlmd5)
            val = '{publisher}::{pubtime}'.format(publisher=publisher,
                                           pubtime=pubtime)
            await self.queue.set(key, val)
            urls.append(link)

        nav_page = soup.select('p#page > a.n')
        if nav_page:
            href = nav_page[-1].get('href')
            rsv_page = re.search(r'rsv_page=(-?\d+)', href)[1]
            if rsv_page == '1':
                pn_new = re.search(r'pn=(\d+)', href)[0]
                pn_old_ret = re.search(r'pn=(\d+)', self.job.url)
                if pn_old_ret:
                    pn_old = pn_old_ret[0]
                    url_next = self.job.url.replace(pn_old, pn_new)
                else:
                    url_next = self.job.url + '&' + pn_new
            else:
                url_next = None
        else:
            url_next = None
        del soup, page
        return (urls, url_next)

    @staticmethod
    def _extract_pubinfo(pubinfo):
        """
        :param pubinfo: str, contains publisher and pubtime
        :rtype (publisher, pubtime)
        """

        publisher, pubtime = '', datetime.utcnow()

        split_args = re.split(r'\s+', pubinfo)
        split_len = len(split_args)
        if split_len == 3:
            publisher = Spider._extract_publisher(split_args[0])
            pubtime = Spider._extract_time(split_args[1], split_args[2])
        elif split_len == 2:
            if ':' in split_args[-1]:
                pubtime = Spider._extract_time(split_args[0], split_args[1])
            else:
                publisher = Spider._extract_publisher(split_args[0])
                pubtime = Spider._extract_time(None, None, split_args[1])
        elif split_len == 1:
            if PAT_YEAR_MONTH_DAY.match(split_args[-1]):
                pubtime = Spider._extract_time(split_args[0])
            elif PAT_SHORT_TIME.match(split_args[-1]):
                pubtime = Spider._extract_time(None, None, split_args[-1])

        pubtime = pubtime.astimezone(tz=pytz.utc)
        pubtime = int(pubtime.timestamp())
        return (publisher, pubtime)

    @staticmethod
    def _extract_publisher(pub_str):
        """
        extract publisher from pub_str
        """
        publisher = PAT_PUBLISHER.match(pub_str).group('publisher')
        return publisher

    @staticmethod
    def _extract_time(ymd_str, hm_str=None, st_str=None):
        """
        extract pubtime from date str
        """
        if st_str:
            t = Spider._extract_st(st_str)
        else:
            year, month, day = Spider._extract_ymd(ymd_str)
            if hm_str:
                hour, minute = Spider._extract_hm(hm_str)
                t = datetime(year, month, day, hour, minute, tzinfo=TZ_SH)
            else:
                t = datetime(year, month, day, tzinfo=TZ_SH)

        return t

    @staticmethod
    def _extract_ymd(ymd_str):
        """
        extract year, month, day from ymd_str
        """
        ymd_ret = PAT_YEAR_MONTH_DAY.match(ymd_str)
        year = int(ymd_ret.group('year'))
        month = int(ymd_ret.group('month'))
        day = int(ymd_ret.group('day'))
        return (year, month, day)

    @staticmethod
    def _extract_hm(hm_str):
        """
        extract hour, minute from hm_str
        """
        hm_ret = PAT_HOUR_MINUTE.match(hm_str)
        hour = int(hm_ret.group('hour'))
        minute = int(hm_ret.group('minute'))
        return (hour, minute)

    @staticmethod
    def _extract_st(st_str):
        """
        extract short time format form st_str
        """
        st_ret = PAT_SHORT_TIME.match(st_str)
        digit = int(st_ret.group('digit'))
        hmtype = st_ret.group('hmtype')
        if hmtype == '分钟':
            t = datetime.now(tz=TZ_SH) - timedelta(seconds=digit * 60)
        elif hmtype == '小时':
            t = datetime.now(tz=TZ_SH) - timedelta(hours=digit)
        return t
