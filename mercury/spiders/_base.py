# -*- coding: utf-8 -*-

import os
import gzip
import asyncio
import logging
import random
from datetime import date
from concurrent.futures import ProcessPoolExecutor

from pymongo.errors import DuplicateKeyError

from mercury.downloader import AsyncHttpDownloader
from mercury.models import News, Job, JobQueue
from mercury.libs import s3
from mercury.settings import logger

from mercury.monitor import (download_failures, extract_detail_failures,
                             news_saved_to_mongo, time_on_download,
                             time_on_write_mongo, time_on_upload_s3,
                             time_on_extract_detail, time_on_extract_index)


cpu_num = os.cpu_count()


class Spider:

    loop = asyncio.get_event_loop()
    # 如果CPU核心数量大于 1 才用进程池
    executor = ProcessPoolExecutor(
        max_workers=cpu_num) if cpu_num > 1 else None
    queue = JobQueue.pool

    def __init__(self, job):
        self.job = job
        anti_cfg = job.anti_cfg
        self.configs = {}
        self.configs.update(job.anti_cfg)
        self.configs.update(self.setup_configs())

    def setup_configs(self):
        """爬虫的配置，子类中只需返回个性化定制的配置项

        :supported config items:

        :config `headers` dict: custom HTTP request headers
        :config `sleep_range_before_request` tuple: (start, stop), sleep
            some time at top of `_make_request`
        """
        raise NotImplementedError("Must setup configurations for spider.")

    async def handle_index(self, page):
        """解析索引页(又称列表页)，能够获取文章详情链接的页面
        如，搜索返回的文章列表、网站子栏目里的文章列表

        返回需要抓取的所有详情链接，分页也在此处理

        :param page: 列表页面内容字符串
        :return: 详情链接列表与下一页网址 ([url1, url2, ...], nextpage_url)
            如果没有下一页，nextpage_url 值为None
        """
        raise NotImplementedError("Must implement how to handle index page.")

    async def handle_detail(self, page, encoding):
        """解析文章详情页，返回相应的详情内容，字段见models中的模块
        :param page: 详情页面内容字符串
        :param encoding: 详情页面内容字符编码
        :return: 代表文章详情的字典
        """
        raise NotImplementedError("Must implement how to handle detail page.")

    async def _make_request(self):
        """构造合适的HTTP请求，并下载页面
        """
        sleep_range = self.configs.get("sleep_range_before_request")
        if sleep_range:
            await asyncio.sleep(random.uniform(*sleep_range))
        if self.configs.get("proxy"):
            proxy = "get proxy from some where"
        if self.configs.get("cookie"):
            should_close_cookie = True
        if self.configs.get("random_ua"):
            ua = "randomly select a User-Agent from somewhere"

        dl = AsyncHttpDownloader(
            self.job.url,
            data=self.configs.get("post_data"),
            headers=self.configs.get("headers"),
            cookies=self.configs.get("cookies")
        )
        page, encoding = await dl.fetch()
        return page, encoding

    async def start(self):
        logger.info("[Spider]<{0}> start crawling url: {1}"
                    .format(self.name, self.job.url))

        ts_start = self.loop.time()
        page, encoding = await self._make_request()
        ts_end = self.loop.time()
        time_on_download.observe(ts_end - ts_start)

        # page 里若无内容认为下载失败，做一次统计，但并不终止处理
        # 给子类在 handle_index 和 handle_detail 中有机会去清理缓存
        if not page:
            download_failures.inc()

        if self.job.is_seed:
            # 本次任务是列表页，则将列表页中解析出的
            # 文章详情 url 生成下载作业并推送到下载队列
            logger.info("[Spider]<{0}> handle index page of: {1}"
                        .format(self.name, self.job.url))

            ts_start = self.loop.time()
            detail_urls, next_url = await self.handle_index(page)
            ts_end = self.loop.time()
            time_on_extract_index.observe(ts_end - ts_start)

            if next_url:
                detail_urls.append(next_url)
                is_seed = True
            else:
                is_seed = False

            while detail_urls:
                url = detail_urls.pop()
                job = Job(
                    url=url,
                    seed_urlmd5=self.job.seed_urlmd5,
                    spider_name=self.job.spider_name,
                    anti_cfg=self.job.anti_cfg,
                    is_seed=is_seed)
                await JobQueue.rpush(job)
                is_seed = False
        else:
            # 如果本次任务下载的是详情页
            logger.info("[Spider]<{0}> handle detail page of: {1}"
                        .format(self.name, self.job.url))

            ts_start = self.loop.time()
            data = await self.handle_detail(page, encoding)
            ts_end = self.loop.time()
            time_on_extract_detail.observe(ts_end - ts_start)

            # 页面有内容，但无目标数据视为抽取失败
            if page and (not data):
                extract_detail_failures.inc()

            if data:
                news = News(**data)
                ts_start = self.loop.time()
                retval = await news.save()
                ts_end = self.loop.time()
                time_on_write_mongo.observe(ts_end - ts_start)

                if isinstance(retval, DuplicateKeyError):
                    return None
                else:
                    news_saved_to_mongo.inc()

                # 入库完成后标记本详情页面已经抓取过
                await self.queue.set(
                    "job:crawled:{0}".format(self.job.urlmd5), 1)

                # 存原始HTML页面到S3服务
                fname = "{spider}/{date}/{urlmd5}.html.gz".format(
                    spider=self.name,
                    date=date.today().strftime("%Y%m%d"),
                    urlmd5=self.job.urlmd5)

                ts_start = self.loop.time()
                # await s3.upload_file(gzip.compress(page.encode(encoding)), fname)
                ts_end = self.loop.time()
                time_on_upload_s3.observe(ts_end - ts_start)
                logger.info("[Spider]<{0}> upload {1} to s3 finished"
                            .format(self.name, fname))


class DefaultSpider(Spider):
    """默认 Spider，做成自动处理的
    """

    def __init__(self, job):
        super().__init__(job)

    def handle_index(self, resp):
        pass

    def handle_detail(self, resp):
        pass
