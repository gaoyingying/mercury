# -*- coding: utf-8 -*-

"""
抓取任务模型，供调度模块使用
"""
import demjson
import logging

from mercury.libs.redis import redis_pool
from mercury.utils import lazyproperty, timestamp, md5
from mercury.settings import logger


PRI_HIGH = 0
PRI_MID = 1
PRI_LOW = 2


class JobQueue:
    """基于 Redis list 的下载任务队列，分为三个优先级队列.
    对应redis key 分别为 "mercury:jobq:high"
                         "mercury:jobq:mid"
                         "mercury:jobq:low"
    """
    pool = redis_pool
    queues = ["mercury:jobq:high", "mercury:jobq:mid", "mercury:jobq:low"]

    @classmethod
    async def blpop(cls, timeout=2):
        """返回一个 job 对象
        """
        retval = await cls.pool.blpop(*cls.queues, timeout=timeout)
        logger.info("[Queue] BLPOP: {0}".format(retval))
        if retval:
            qname, job_str = retval
            return Job.deserialization(job_str)

    @classmethod
    async def rpush(cls, job):
        # 检查job是否已经被抓取过
        is_crawled = await cls.pool.get(
            "job:crawled:{0}".format(job.urlmd5))
        if is_crawled:
            logger.info("[Queue] DUPLICATE: {0} this job already crawled"
                        .format(job.urlmd5))
            return None
        data = job.__dict__.copy()
        data["queued_time"] = timestamp(is_int=True)
        data = demjson.encode(data)
        await cls.pool.rpush(cls.queues[job.priority], data)
        logger.info("[Queue] RPUSH: {0}".format(data))

    @classmethod
    async def total_len(cls):
        counter = 0
        for key in cls.queues:
            counter += await cls.pool.llen(key)
        logger.info("[Queue] Total number of jobs remaining: {0}"
                    .format(counter))
        return counter


class Job:
    def __init__(self, *, url, seed_urlmd5, spider_name,
                 anti_cfg, is_seed=False, max_tries=2,
                 priority=PRI_MID, **kwargs):
        """抓取任务

        :param url: 待抓取任务的url
        :param priority: 表示抓取优先级，PRI_LOW|PRI_MID|PRI_HIGH
        """
        self.url = url
        self.urlmd5 = md5(url)
        self.seed_urlmd5 = seed_urlmd5
        self.spider_name = spider_name
        self.anti_cfg = anti_cfg
        self.is_seed = is_seed
        self.priority = priority
        self.max_tries = max_tries
        self.queued_time = kwargs.pop("queued_time", None)
        # job用完即销，暂时不用记录状态
        # self.status = kwargs.pop("status", None)

    @classmethod
    def deserialization(cls, obj_str):
        data = demjson.decode(obj_str)
        return cls(**data)

    # 暂时按三个优先级区分

    # @property
    # def priority(self):
        # return self._priority

    # @priority.setter
    # def priority(self, value):
        # """检查value的有效性，整数
        # 更新任务队列里的既有配置
        # """
        # self._priority = value


