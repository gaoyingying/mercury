# -*- coding: utf-8 -*-

"""
新闻数据的存储模型
"""
import logging

from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError

from mercury.libs.mongo import mgdb
from mercury.utils import md5, timestamp, lazyproperty
from mercury.settings import API_VIBRIO_GEN, logger
from mercury.monitor import news_duplicated


class News:

    _db = mgdb.news

    def __init__(self, url, title, content, **kwargs):
        self._id = None
        # self.nid = "create_from_vibrio"             # 统一ID
        self.url = url                              # 新闻URL
        self.urlmd5 = md5(url)                      # URL MD5哈希值
        self.title = title                          # 正文标题
        self.content = content                      # 正文内容
        self.page = kwargs.get("page", None)              # HTML原始页面存储位置
        self.author = kwargs.pop("author", None)          # 文章作者 eg. 张三
        self.publisher = kwargs.pop("publisher", None)    # 文章出版者 eg. 人民日报
        self.getway = kwargs.pop("getway", None)          # 获取/发现途径 eg. 百度新闻
        self.pubtime = kwargs.pop("pubtime", None)        # 文章发布时间
        # self.origin = kwargs.pop("origin")         # 文章原始出处
        self.create_time = timestamp()              # 此新闻在本系统的创建时间
        self.smhash = None                          # 正文内容的superminhash值
        self.sid = kwargs.pop("sid", None)                # seed id

    @classmethod
    async def get_by_nid(cls):
        """通过nid从数据库中查询新闻，并返回 News 对象
        """
        pass

    async def load_to_cache(self):
        """将新闻linkmd5加载入缓存

        ps. 暂时写这里，之后考虑移到专门的缓存管理模块
        """

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        return data

    async def update(self, data=None):
        if not data:
            data = self.to_mongo()
        logger.info("[DB]News updating: {0}".format(data["url"]))
        retval = await self._db.update_one(
            {'_id': self._id}, {'$set': data})
        logger.debug("[DB]News updated: {0}".format(data))
        return data

    async def save(self):
        data = self.to_mongo()
        try:
            logger.info("[DB]News inserting: {0}".format(data["url"]))
            result = await self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            news_duplicated.inc()
            logger.info("[DB]News already exists {0} {1}"
                        .format(self.urlmd5, self.url))
            retval = e
        else:
            data["_id"] = str(self._id)
            retval = data
            logger.debug("[DB]News inserted: {0}".format(retval))
        return retval

    async def remove(self):
        pass
