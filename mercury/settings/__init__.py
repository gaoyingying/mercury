# -*- coding: utf-8 -*-

import os
import time
import logging
import logging.config

import yaml


path = lambda root, *a: os.path.join(root, *a)

# ####################
# Basic paths
# ####################
DIR_SETTING = os.path.dirname(os.path.abspath(__file__))
DIR_PROJECT = os.path.dirname(os.path.dirname(DIR_SETTING))


# ####################
# Deployment types
# ####################
class DeployType:
    PROD = "PRODUCTION"
    TEST = "TEST"
    DEV = "DEVELOPMENT"

if "DEPLOYTYPE" in os.environ:
    DEPLOYTYPE = os.environ["DEPLOYTYPE"].upper()
else:
    DEPLOYTYPE = DeployType.DEV

# ####################
# Third Services
# ####################
API_VIBRIO_GEN = "http://wait.delpoy.com/generator"
API_VIBRIO_RES = "http://wait.deploy.com/resolver"


# ####################
# PyYAML pre-settings
# ####################
def __pathjoin(loader, node):
    seq = loader.construct_sequence(node)
    return path(*seq)

yaml.add_constructor("!pathjoin", __pathjoin)


# ####################
# Load storage config
# ####################
STORAGE = {}

with open(path(DIR_SETTING, "storage.yml")) as f:
    _storageconf = yaml.safe_load(f)
    # 删除无关配置
    for ty in {DeployType.PROD, DeployType.TEST, DeployType.DEV} - {DEPLOYTYPE}:
        del _storageconf[ty]
    STORAGE = _storageconf.pop(DEPLOYTYPE)


# ####################
# Logging configurations
# ####################

# use UTC time
logging.Formatter.converter = time.gmtime

with open(path(DIR_SETTING, "logging.yml")) as f:
    _logconf = yaml.load(f)
    _logconf_p = _logconf.pop(DeployType.PROD.lower())
    _ = _logconf.pop("log_dir")   # 后续无用
    if DEPLOYTYPE == DeployType.PROD:
        _logconf.update(_logconf_p)
    logging.config.dictConfig(_logconf)

# disable verbose logging
logging.getLogger('nose').setLevel(logging.WARNING)
logging.getLogger('aioredis').setLevel(logging.INFO)
logging.getLogger('aiokafka').setLevel(logging.INFO)
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)
logging.getLogger('chardet.charsetprober').setLevel(logging.WARNING)
logging.getLogger('jieba').setLevel(logging.WARNING)

logger = logging.getLogger('mercury.main')


# ####################
# RuoKuai captcha identification
# ####################
class RuoKuaiConf:
    username = "metasota"
    password = "MetaSot4!"
    softid = "106594"
    softkey = "55c8ec599a784eedba18f6815afbbf4c"
    FOUR_DIGIT_LETTER = "3040"    # 4位英数混合
    SIX_DIGIT_LETTER = "3060"     # 6位英数混合
    SIX_LETTER = "2060"           # 6位英文


# ####################
# Constant variables
# ####################
RK_BDNEWS_PUBINFO = 'bdnews:pubinfo:{urlmd5}'

# 单个进程同时下载并处理的job数量
MAX_JOBS = 6



if __name__ == "__main__":
    print(STORAGE)
    logger = logging.getLogger("mercury.main")
    logger.info("test")
