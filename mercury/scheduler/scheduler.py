# -*- coding: utf-8 -*-

import asyncio
import logging
import random

from mercury.models import Seed, Job, JobQueue, News
from mercury.utils import timestamp
from mercury.spiders import (WeixinSpider,
                             DefaultSpider,
                             BaiduNewsSpider)
from mercury.settings import logger, MAX_JOBS
from mercury.monitor import (jobs_in_redis, news_amount, seeds_amount,
                             zero_seeds_amount, jobs_inprogress,
                             enter_processing)


spiders = {
    "weixin": WeixinSpider,
    "baidunews": BaiduNewsSpider,
    "default": DefaultSpider
}


class Scheduler:
    """调度类
    """

    def __init__(self):
        pass

    async def _get_jobs(self, num=30):
        """批量从抓取队列获取抓取任务
        """
        pass

    @classmethod
    async def dispatch(cls, job):
        """将下载任务分发给对应的spider
        """
        # 分发任务前进行一次重复检查，若重复则不分发
        is_crawled = await JobQueue.pool.get(
            "job:crawled:{0}".format(job.urlmd5))
        if is_crawled:
            logger.info("[Sched] DUPLICATE: job.urlmd5:{0}".format(job.urlmd5))
            return None

        spider_name = job.spider_name or "default"
        Spider = spiders.get(spider_name)
        logger.info("[Sched]Dispatch job.urlmd5:{0} to spider: {1}"
                    .format(job.urlmd5, spider_name))
        jobs_inprogress.inc()
        enter_processing.inc()
        await Spider(job).start()
        jobs_inprogress.dec()

    @classmethod
    async def check_new_seeds(cls, interval=300):
        """检查数据库中是否有新种子加入，如果有则将
        新种子载入下载队列

        :param int interval: 每隔 interval 秒检查一次
        """
        # (当前时间 - 调度间隔)) >= 上次调度时间
        # 且 状态为OK的种子
        # ps. MongoDB 中Date对象为毫秒级
        should_sched = ("(new Date(ISODate().getTime()"
                        "- this._sched_interval*1000)"
                        ">= this.last_sched_time)"
                        "&& this.status=='OK'")
        while True:
            await asyncio.sleep(interval)

            # 队列中下载作业数大于3000时不调度新种子
            jobs_left = await JobQueue.total_len()
            if jobs_left > 3000:
                continue
            # 在应该调度种子时，先随机sleep一段时间，
            # 一定程度避免多个节点同时发起mongo查询请求而调度过多种子
            await asyncio.sleep(random.uniform(10, 20))
            logger.info("[Sched]Checking new seeds ...")
            async for seed in Seed.find(
                {"$where": should_sched},
                # 抓取次数少的第一优先，创建时间早的第二优先
                sort=[("crawled_times", 1), ("create_time", 1)]):

                job = Job(
                    url=seed.url,
                    seed_urlmd5=seed.urlmd5,
                    spider_name=seed.getway,
                    anti_cfg=seed.anti_cfg,
                    is_seed=True)
                logger.info("[Sched]Seed to queue, URL {0}, GETWAY {1}"
                            .format(seed.url, seed.getway))
                await JobQueue.rpush(job)
                seed.last_sched_time = timestamp()
                seed.crawled_times += 1
                await seed.update()

    @classmethod
    async def run(cls):
        while True:
            await cls._record_status()
            logger.info("[Sched]Getting jobs from queue")
            # 给种子检查、API等协程多点执行机会
            await asyncio.sleep(0)
            jobs = await asyncio.gather(
                *[JobQueue.blpop() for _ in range(MAX_JOBS)])
            jobs = list(filter(None, jobs))
            if jobs:
                # 分发任务至具体的 Spider
                await asyncio.gather(*[cls.dispatch(job) for job in jobs])
            else:
                logger.info("[Sched]No download job yet")

    @staticmethod
    async def _record_status():
        """recording prometheus metrics
        """
        jobs_in_redis.set(await JobQueue.total_len())

        news_amount.set(await News._db.count())

        seeds_amount.labels(getway="baidunews").set(
            await Seed._db.count({"getway": "baidunews"}))

        zero_seeds_amount.labels(getway="baidunews").set(
            await Seed._db.count({"getway": "baidunews", "crawled_times": 0}))
