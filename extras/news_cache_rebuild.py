# -*- coding: utf-8 -*-


import asyncio

import sys
sys.path.append(r'../mercury')

from mercury.libs.mongo import mgdb
from mercury.libs.redis import redis_pool


RK = 'job:crawled:{urlmd5}'


async def load_crawled_jobs_to_cache():
    """
    query data from mongodb and insert into redis
    """
    cursor = mgdb.news.find({}, {'urlmd5': 1, '_id': 0}).sort([('create_time', 1)])
    cursor.batch_size(4096)
    cnt = 0
    async for doc in cursor:
        await redis_pool.set(RK.format(urlmd5=doc['urlmd5']), '1')
        cnt += 1
        if cnt % 100==0:
            print("{} items was inserted".format(cnt))

    print('total inserted {} items'.format(cnt))


def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(load_crawled_jobs_to_cache())


if __name__ == '__main__':
    main()

