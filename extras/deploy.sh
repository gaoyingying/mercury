#!/bin/bash

set -e;
set -x;
cd ~;

# 设置环境变量
echo -e "\\nexport LANG=en_US.utf8\\nexport LC_ALL=en_US.utf8\\nexport PATH=\"$PATH:/usr/local/python3.6/bin\"\\nexport DEPLOYTYPE=\"PRODUCTION\"" >> /etc/profile;
# shellcheck disable=SC1091
source /etc/profile;

# 安装开发依赖
apt-get update;
apt install -y build-essential;
add-apt-repository -y ppa:ubuntu-toolchain-r/test;
apt-get update;
apt install -y gcc-6 g++-6;

update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 20;
update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 50;
update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 20;
update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-6 50;

# 安装 supervisor 、fail2ban、htop、ccze 系统应用(此时python2.7会被作为依赖安装，系统中默认的Python3.5不变)
apt install -y supervisor fail2ban htop ccze
echo "supervisor and fail2ban installed"

# swap 设置
if [[ $(swapon --show) ]]; then
    echo "Already has swap"
else
    fallocate -l 2G /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    echo "/swapfile none swap sw 0 0" | tee -a /etc/fstab
    echo -e "vm.swappiness = 20\\nvm.vfs_cache_pressure = 60" | tee -a /etc/sysctl.conf
    sysctl -p
    if [[ $(swapon --show) ]]; then
        echo "swap set successful"
    else
        echo "swap set failed, plz check"
    fi
fi

# 创建所需文件夹
mkdir -p /opt/venvs /opt/metasota
mkdir -p /data/log/mercury /data/log/supervisor

# 下载已编译的 Python3.6，注意更新下载地址，验证有效期为5天
wget -O python-3.6-ubuntu64.tar.xz "http://54.39.104.2:9999/common/python-3.6-ubuntu64.tar.xz?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=WX3RN80XHC5XPKRVOLGW%2F20180613%2F%2Fs3%2Faws4_request&X-Amz-Date=20180613T081606Z&X-Amz-Expires=432000&X-Amz-SignedHeaders=host&X-Amz-Signature=788b86ffce7a2461a4760f62b60e8ebcffc4d8df6346e0ad88833a40fd496f45"

# 安装 Python3.6
tar -Jxf python-3.6-ubuntu64.tar.xz;
mv python3.6 /usr/local/;
echo "/usr/local/python3.6/lib" > /etc/ld.so.conf.d/python36.conf;
ldconfig

PY=$(python3.6 --version)
if [[ $PY ]]; then
    echo "$PY has been installed"
    # 下载和安装 mercury
    python3.6 -m venv /opt/venvs/mercury
    git clone -b develop https://denglj@bitbucket.org/meta-sota/mercury.git /opt/metasota/mercury

    # shellcheck disable=SC1091
    source /opt/venvs/mercury/bin/activate
    cd /opt/metasota/mercury
    rm -f requirements/*.txt
    make deps
    make pip
    deactivate

    sed -i "s/\\/var\\/log\\/supervisor/\\/data\\/log\\/supervisor/g" /etc/supervisor/supervisord.conf
    cp extras/supervisor.conf /etc/supervisor/conf.d/mercury.conf

    # 清理内存
    swapoff -a
    swapon -a
    sync
    echo 1 > /proc/sys/vm/drop_caches
    echo 2 > /proc/sys/vm/drop_caches
    echo 3 > /proc/sys/vm/drop_caches

    # 重载 supervisor 自动启动 mercury
    supervisorctl reload
    # 等待启动完成
    sleep 4
    nproc=$(pgrep -c python)
    if [[ $nproc ]]; then
        echo "$nproc mercury processes is runing"
    else
        echo "mercury runing failed, plz check"
    fi
else
    echo "Python3.6 installation failed, plz check"
fi